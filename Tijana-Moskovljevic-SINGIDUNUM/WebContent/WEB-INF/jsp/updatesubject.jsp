
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update subject</title>
<h3>Update subject</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
    var array = [];
	function changeProfessor() {
		var professor = document.getElementById("thatProfessor").value
		array.push(professor);
		document.getElementsByName("listOfProf")[0].value = array;
	}
	
</script>
</head>
<body>
	<form action="${pageContext.request.contextPath}/doupdatesubject"
		method="post">
			<tr>
				<td class="label"></td>
				<td><input type="hidden" name="subjectId"
					value="${subject.subjectId}"></td>
			</tr>
			<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="label">Name</td>
				<td><input type="text" name="name" value="${subject.name}"
					minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="label">Description</td>
				<td><input type="text" name="description"
					value="${subject.description}" maxlength="200" required></td>
			</tr>
			<tr>
				<td class="label">YearOfStudy</td>
				<td><input type="number" name="yearOfStudy"
					value="${subject.yearOfStudy}" min="1" , max="9" required></td>
			</tr>
			<tr>
				<td class="label">Semester</td>
				<td><input type="text" name="semester"
					value="${subject.semester}" required></td>
			</tr>
			<tr>
				<td class="label">Professors:</td>
				<td><select name="professorId" id="thatProfessor">
						<c:forEach var="tempProfessor" items="${professors}">
							<option value="${tempProfessor.professorId}">${tempProfessor.firstName}
								${tempProfessor.lastName}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label">Edit student:</td>
				<td><button onclick="changeProfessor()">Update subject</button></td>
			</tr>
			 <tr>
				<td class="label"></td>
				<td><input name="listOfProf" value=""></td>
			</tr>
		</table>
	</form>
			<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/subjects">Back
			to previous page</button>
	</form>
</body>
</html>