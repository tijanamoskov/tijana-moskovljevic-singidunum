<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all professors</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
function confirmDelete(i){
	var c=confirm("Are you sure you want to delete the professor")
	if(c==true){
		window.location.pathname="${pageContext.request.contextPath}/deleteprofessor/"+i;
	}
}
</script>
</head>
<body>
	<h3>All professors:</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>ProfessorId</th>
				<th>FirstName</th>
				<th>LastName</th>
				<th>Email</th>
				<th>Address</th>
				<th>Phone</th>
				<th>ReelectionDate</th>
				<th>City</th>
				<th>Title</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach var="tempProfessor" items="${professors}">
					<tr>
						<td>${tempProfessor.professorId}</td>
						<td>${tempProfessor.firstName}</td>
						<td>${tempProfessor.lastName}</td>
						<td>${tempProfessor.email}</td>
						<td>${tempProfessor.adress}</td>
						<td>${tempProfessor.phone}</td>
						<td>${tempProfessor.reelectionDate}</td>
						<td>${tempProfessor.city}</td>
						<td>${tempProfessor.title}</td>
						<td><a
							href="${pageContext.request.contextPath}/updateprofessor/${tempProfessor.professorId}">updateprofessor</a></td>
						<td><a onclick="confirmDelete(${tempProfessor.professorId})">Delete</a></td>
					</tr>
				</c:forEach>
			</tr>
		</tbody>
	</table>
			<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>
