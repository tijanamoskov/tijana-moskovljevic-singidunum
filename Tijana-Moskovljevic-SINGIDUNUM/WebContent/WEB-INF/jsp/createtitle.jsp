<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new city</title>
<h3>Add new title</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<form action="${pageContext.request.contextPath}/docreatetitle"
		method="post">
		<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="Label">Title Name:</td>
				<td class="control"><input name="name" type="text" required></td>
			</tr>
			<tr>
				<td class="Label"></td>
				<td class="control"><input type="submit" value="add new title"></td>
			</tr>
		</table>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK
			TO HOME PAGE</button>
	</form>
</body>
</html>