<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new student</title>
<h3>Add new student</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
	var array = [];
	function newSubject() {
		var subject = document.getElementById("selectedSubject").value
		array.push(subject);
		document.getElementsByName("listOfSubjects")[0].value = array;
	}
</script>
</head>
<body>
	<form action="${pageContext.request.contextPath}/docreatestudent"
		method="post">
		<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="Label">IndexNumber:</td>
				<td class="control"><input name="indexNumber" type="text"
					maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">First Name:</td>
				<td class="control"><input name="firstName" type="text"
					minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Last Name</td>
				<td class="control"><input name="lastName" type="text"
					minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Email</td>
				<td class="control"><input name="email" type="email"
					maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Address</td>
				<td class="control"><input name="adress" type="text"
					minlength="3" maxlength="50" required></td>
			</tr>
			<tr>
				<td class="Label">Phone</td>
				<td class="control"><input name="phone" type="text"
					minlength="6" maxlength="15" required></td>
			</tr>
			<tr>
				<td class="Label">CurrentYearOfStudy</td>
				<td class="control"><input name="currentYearOfStudy"
					type="number" min="1" max="7" required></td>
			</tr>
			<tr>
				<td class="Label">City</td>
				<td><select name="cityId">
						<option></option>
						<c:forEach var="tempCity" items="${cities}">
							<option value="${tempCity.cityId} ">${tempCity.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label">Subjects:</td>
				<td><select name="subjectId" id="selectedSubject">
						<option></option>
						<c:forEach var="tempSubject" items="${subjects}">
							<option value="${tempSubject.subjectId}">${tempSubject.name}
							</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label">Add subject:</td>
				<td><input type="button" onclick="newSubject()"></td>
			</tr>
			<tr>
				<td class="label">Added subject</td>
				<td><input name="listOfSubjects" value=""></td>
			</tr>
			<tr>
				<td class="Label"></td>
				<td class="control"><input type="submit"
					value="create new student"></td>
			</tr>
		</table>
	</form>
			<br>		
	   <form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>