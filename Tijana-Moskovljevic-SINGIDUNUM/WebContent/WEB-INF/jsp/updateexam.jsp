<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Exam</title>
<h3>Update exam</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<form action="${pageContext.request.contextPath}/doupdateexam"
		method="post">
		
			<tr>
				<td class="label"></td>
				<td><input type="hidden" name="examId" value="${exam.examId}"></td>
			</tr>
			<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="label">ExamDate</td>
				<td><input type="date" name="examdate"
					value="${exam.examdate}"></td>
			</tr>
			<tr>
				<td class="label">Professors</td>
				<td><select name="professorId">
               <c:forEach var="tempProfessor" items="${professors}">
							<option value="${tempProfessor.professorId}">${tempProfessor.firstName} ${tempProfessor.lastName}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="label">Subjects</td>
				<td><select name="subjectId">
						<c:forEach var="tempSubject" items="${subjects}">
							<option value="${tempSubject.subjectId}">${tempSubject.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="label"></td>
				<td class="control"><input type="submit" value="Update"></td>
			</tr>

		</table>
	</form>
		<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/exams">Back
			to previous page</button>
	</form>
</body>
</html>