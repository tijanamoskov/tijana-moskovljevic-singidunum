<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Professor</title>
<h3>Update professor</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<form action="${pageContext.request.contextPath}/doupdateprofessor"
		method="post">
			<tr>
				<td class="label"></td>
				<td><input type="hidden" name="professorId"
					value="${professor.professorId}"></td>
			</tr>
			<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="label">FirstName</td>
				<td><input type="text" name="firstName"
					value="${professor.firstName}" minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="label">LastName</td>
				<td><input type="text" name="lastName"
					value="${professor.lastName}" minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="label">Email</td>
				<td><input type="text" name="email" value="${professor.email}" required></td>
			</tr>
			<tr>
				<td class="label">Address</td>
				<td><input type="text" name="adress"
					value="${professor.adress}" minlength="3" maxlength="50" required></td>
			</tr>
			<tr>
				<td class="label">Phone</td>
				<td><input type="text" name="phone" value="${professor.phone}" minlength="6" maxlength="15" required></td>
			</tr>
			<tr>
				<td class="label">ReelectionDate</td>
				<td><input type="date" name="reelectionDate"
					value="${professor.reelectionDate}" required></td>
			</tr>
			<tr>
			    <td class="label">City</td>
				<td><select name="cityId">
						<c:forEach var="tempCity" items="${cities}">
							<option value="${tempCity.cityId}">${tempCity.name}</option>
						</c:forEach>
				</select>
				</td>
			</tr>
			<tr>
			<td class="label">Title</td>
			<td><select name="titleId">
			<c:forEach var="tempTitle" items="${titles}">
			<option value="${tempTitle.titleId}">${tempTitle.name}</option>		
			</c:forEach>
			</select>		
			</td>
			</tr>
			<tr>
			<td class="label"></td>
			<td class="control"><input type="submit" value="Update"></td>
			</tr>
		</table>
	</form>
		<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/professors">Back
			to previous page</button>
	</form>
</body>
</html>