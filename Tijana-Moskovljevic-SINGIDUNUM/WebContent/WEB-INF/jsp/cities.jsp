<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all cities</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
function confirmDelete(i){
	var c=confirm("Are you sure you want to delete the city")
	if(c==true){
		window.location.pathname="${pageContext.request.contextPath}/deletecity/"+i;
	}
}
</script>
</head>
<body>
	<h3>All cities:</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>CityId</th>
				<th>Name</th>
				<th>Postal Code</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach var="tempCity" items="${cities}">
					<tr>
						<td>${tempCity.cityId}</td>
						<td>${tempCity.name}</td>
						<td>${tempCity.postalCode}</td>
						<td><a
							href="${pageContext.request.contextPath}/updatecity/${tempCity.cityId}">updatecity</a></td>
						<td><a onclick="confirmDelete(${tempCity.cityId})">Delete</a></td>
					</tr>
				</c:forEach>
			</tr>
		</tbody>
	</table><br>
		<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>
