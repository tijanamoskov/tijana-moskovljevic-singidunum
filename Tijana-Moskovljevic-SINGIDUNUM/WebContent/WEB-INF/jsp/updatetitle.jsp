<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<title>Update title</title>
<h3>Update title</h3>
</head>
<body>
	<form action="${pageContext.request.contextPath}/doupdatetitle"
		method="post">
			<tr>
				<td class="label"></td>
				<td><input type="hidden" name="titleId"
					value="${title.titleId}"></td>
			</tr>
			<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="label">Title Name</td>
				<td><input type="text" name="name" value="${title.name}"
					required></td>
			</tr>
			<tr>
				<td class="label"></td>
				<td class="control"><input type="submit" value="Update"></td>
			</tr>
		</table>
	</form>
			<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/titles">Back
			to previous page</button>
	</form>
</body>
</html>