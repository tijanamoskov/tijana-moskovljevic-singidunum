<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all titles</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
    type="text/css" rel="stylesheet" >
<script type="text/javascript">
function confirmDelete(i){
	var c=confirm("Are you sure you want to delete the title")
	if(c==true){
		window.location.pathname="${pageContext.request.contextPath}/deletetitle/"+i;
	}
}
</script>
</head>
<body>
	<h3>All titles:</h3>
	<table border="2" cellpading="10" cellspacing="2">
		<thead>
			<tr>
				<th>TitleId</th>
				<th>Title Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach var="tempTitle" items="${titles}">
					<tr>
						<td>${tempTitle.titleId}</td>
						<td>${tempTitle.name}</td>
						<td><a
							href="${pageContext.request.contextPath}/updatetitle/${tempTitle.titleId}">updatetitle</a></td>
						<td><a onclick="confirmDelete(${tempTitle.titleId})">Delete</a></td>
					</tr>
				</c:forEach>
			</tr>
		</tbody>
	</table>
					<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>
