<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home page</title>
<h3>Students service</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	type="text/css" rel="stylesheet">
</head>
<body>
	<form>
		<button formaction="${pageContext.request.contextPath}/createstudent">Create
			new student</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/students">Show
			all students</button>
	</form>
	<br>
	<form>
		<button
			formaction="${pageContext.request.contextPath}/createprofessor">Create
			new professor</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/professors">Show
			all professors</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createtitle">Create
			title</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/titles">Show
			all titles</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createsubject">Create
			new subject</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/subjects">Show
			all subjects</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createexam">Create
			new exam</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/exams">Show
			all exams</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createcity">Create
			city</button>
	</form>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/cities">Show
			all cities</button>
	</form>
	<br>
</body>
</html>