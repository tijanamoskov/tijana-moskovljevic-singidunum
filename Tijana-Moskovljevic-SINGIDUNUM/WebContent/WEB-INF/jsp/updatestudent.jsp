<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Student</title>
<h3>Update student</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
    var array = [];
	function changeSubject() {
		var subject = document.getElementById("thatSubject").value
		array.push(subject);
		document.getElementsByName("listOfSubjects")[0].value = array;
	}
	
</script>
</head>
<body>
	<form action="${pageContext.request.contextPath}/doupdatestudent"
		method="post">
			<tr>
				<td class="Label"></td>
				<td><input type="hidden" name="studentId"
					value="${student.studentId}" /></td>
			</tr>
			<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="Label">IndexNumber:</td>
				<td class="control"><input name="indexNumber" type="text"
					value="${student.indexNumber}" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">First Name:</td>
				<td class="control"><input name="firstName" type="text"
					value="${student.firstName}" minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Last Name</td>
				<td class="control"><input name="lastName" type="text"
					value="${student.lastName}" minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Email</td>
				<td class="control"><input name="email" type="email"
					value="${student.email}" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Address</td>
				<td class="control"><input name="adress" type="text"
					value="${student.adress}" minlength="3" maxlength="50" required></td>
			</tr>
			<tr>
				<td class="Label">Phone</td>
				<td class="control"><input name="phone" type="text"
					value="${student.phone}" minlength="6" maxlength="15" required></td>
			</tr>
			<tr>
				<td class="Label">CurrentYearOfStudy</td>
				<td class="control"><input name="currentYearOfStudy"
					type="number" value="${student.currentYearOfStudy}" min="1" max="7"
					required></td>
			</tr>
			<tr>
			<td class="Label">City</td>
				<td><select name="cityId">
						<c:forEach var="tempCity" items="${cities}">
							<option value="${tempCity.cityId} ">${tempCity.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label">Subjects:</td>
				<td><select name="subjectId" id="thatSubject">
						<c:forEach var="tempSubject" items="${subjects}">
							<option value="${tempSubject.subjectId}">${tempSubject.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label">Edit student:</td>
				<td><button onclick="changeSubject()">Update student</button></td>
			</tr>
			 <tr>
				<td class="label"></td>
				<td><input name="listOfSubjects" value=""></td>
			</tr>
		</table>
	</form>
			<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/students">Back
			to previous page</button>
	</form>
</body>
</html>