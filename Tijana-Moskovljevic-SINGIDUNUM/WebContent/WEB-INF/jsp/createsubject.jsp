<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add new subject</title>
<link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
var array=[];
function newProfessor(){
	var professor= document.getElementById("selectedProfessor").value
	array.push(professor);
	document.getElementsByName("listOfProfessors")[0].value=array;
	}
</script>
</head>
<h3>Add new subject</h3>
<body>
<form action="${pageContext.request.contextPath}/docreatesubject" method="post">
<table border="2" cellpadding="5" cellspacing="2">
<tr>
<td class="Label">Name:</td>
<td class ="control"><input name="name" type="text" minlength="3" maxlength="30" required></td>
</tr>
<tr>
<td class="Label">Description:</td>
<td class ="control"><input name="description" type="text" maxlength="200" required></td>
</tr>
<tr>
<td class="Label">YearOfStudy:</td>
<td class ="control"><input name="yearOfStudy" type="number" min="1" max="9" required></td>
</tr>
<tr>
<td class="Label">Semester:</td>
<td><select name="semester">
<option value="WINTER">WINTER</option>
<option value="SUMMER">SUMMER</option>
</select></td>
</tr>
<tr>
<td class="Label">Professors:</td>
<td><select name="professorId" id="selectedProfessor">
<option></option>
    <c:forEach var="tempProfessor" items="${professors}">  
     <option value="${tempProfessor.professorId}">${tempProfessor.firstName} ${tempProfessor.lastName}</option>
</c:forEach>
</select>
</td>
</tr>
<tr>
	<td class="Label">Add professor:</td>
	<td><input type="button" onclick="newProfessor()"></td>
</tr>
<tr>
	<td class="label">Added professor</td>
	<td><input name="listOfProfessors" value=""></td>
</tr>
<tr>
<td class="Label">New subject:</td>
<td class ="control"><input type="submit" value="add new subject"></td>
</tr>
</table>

</form> 
			<br>		
	   <form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>