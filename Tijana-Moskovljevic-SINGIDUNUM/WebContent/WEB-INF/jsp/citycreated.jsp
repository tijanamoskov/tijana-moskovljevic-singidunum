<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City created</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<h3>City created</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>CityId</th>
				<th>Name</th>
				<th>PostalCode</th>
			</tr>
		</thead>
		<tr>
			<td><c:out value="${city.cityId}"></c:out></td>
			<td><c:out value="${city.name}"></c:out></td>
			<td><c:out value="${city.postalCode}"></c:out></td>
		</tr>
	</table>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createcity">Back
			to previous page</button>
	</form>
	<br>
</body>
</html>