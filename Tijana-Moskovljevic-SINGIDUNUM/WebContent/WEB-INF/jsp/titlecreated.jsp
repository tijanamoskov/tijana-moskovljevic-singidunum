<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	type="text/css" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Title created</title>
</head>
<body>
	<h3>Title created successfully</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>TitleId</th>
				<th>Title Name</th>
			</tr>
		</thead>
		<tr>
			<td><c:out value="${title.titleId}"></c:out></td>
			<td><c:out value="${title.name}"></c:out></td>
		</tr>
	</table>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createtitle">Back
			to previous page</button>
	</form>
</body>
</html>