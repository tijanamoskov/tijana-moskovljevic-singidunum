<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<title>Student created</title>
</head>
<body>
	<h3>Student created successfully</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>StudentId</th>
				<th>IndexNumber</th>
				<th>FirstName</th>
				<th>LastName</th>
				<th>Email</th>
				<th>Address</th>
				<th>Phone</th>
				<th>CurrentYearOfStudy</th>
				<th>City</th>
			</tr>
		</thead>
		<tr>
			<td><c:out value="${student.studentId}"></c:out></td>
			<td><c:out value="${student.indexNumber}"></c:out></td>
			<td><c:out value="${student.firstName}"></c:out></td>
			<td><c:out value="${student.lastName}"></c:out></td>
			<td><c:out value="${student.email}"></c:out></td>
			<td><c:out value="${student.adress}"></c:out></td>
			<td><c:out value="${student.phone}"></c:out></td>
			<td><c:out value="${student.currentYearOfStudy}"></c:out></td>
			<td><c:out value="${student.city}"></c:out></td>

		</tr>
	</table>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createstudent">Back
			to previous page</button>
	</form>
</body>
</html>