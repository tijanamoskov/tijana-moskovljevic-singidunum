<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all exams</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script type="text/javascript">
function confirmDelete(i){
	var c=confirm("Are you sure you want to delete the exam")
	if(c==true){
		window.location.pathname="${pageContext.request.contextPath}/deleteexam/"+i;
	}
}

</script>
</head>
<body>
	<h3>All exams:</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>ExamId</th>
				<th>ExamDate</th>
				<th>Professors</th>
				<th>Subjects</th>
				<th>Edit</th>
				<th>Delete</th>

			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach var="tempExam" items="${exams}">
					<tr>
						<td>${tempExam.examId}</td>
						<td>${tempExam.examdate}</td>
						<td>${tempExam.professor.firstName} ${tempExam.professor.lastName}</td>
						<td>${tempExam.subject.name}</td>
						<td><a href="${pageContext.request.contextPath}/updateexam/${tempExam.examId}">updateexam</a></td>
						<td><a onclick="confirmDelete(${tempExam.examId})">Delete</a></td>
					</tr>
				</c:forEach>
			</tr>
		</tbody>
	</table>
			<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>