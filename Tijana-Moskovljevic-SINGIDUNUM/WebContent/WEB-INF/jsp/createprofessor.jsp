<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<title>Add new professor</title>
<h3>Add new professor</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<form action="${pageContext.request.contextPath}/docreateprofessor"
		method="post">
		<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="Label">First Name:</td>
				<td class="control"><input name="firstName" type="text"
					minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Last Name</td>
				<td class="control"><input name="lastName" type="text"
					minlength="3" maxlength="30" required></td>
			</tr>
			<tr>
				<td class="Label">Email</td>
				<td class="control"><input name="email" type="email" required></td>
			</tr>
			<tr>
				<td class="Label">Address</td>
				<td class="control"><input name="adress" type="text"
					minlength="3" maxlength="50" required></td>
			</tr>
			<tr>
				<td class="Label">Phone</td>
				<td class="control"><input name="phone" type="text"
					minlength="6" maxlength="15" required></td>
			</tr>
			<tr>
				<td class="Label">ReelectionDate</td>
				<td class="control"><input name="reelectionDate" type="date"
					required></td>
			</tr>
			<tr>
				<td class="Label">City</td>
				<td><select name="cityId">
						<option></option>
						<c:forEach var="tempCity" items="${cities}">
							<option value="${tempCity.cityId}">${tempCity.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label">Title</td>
				<td><select name="titleId">
						<c:forEach var="tempTitle" items="${titles}">
							<option value="${tempTitle.titleId} ">${tempTitle.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td class="Label"></td>
				<td class="control"><input type="submit"
					value="create new professor"></td>
			</tr>
		</table>
	</form>
			<br>		
	   <form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>