<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Subject created</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<h3>Subject created successfully</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>SubjectId</th>
				<th>Name</th>
				<th>Description</th>
				<th>YearOfStudy</th>
				<th>Semester</th>
				<th>Professors</th>
			</tr>
		</thead>
		<tr>
			<td><c:out value="${subject.subjectId}"></c:out></td>
			<td><c:out value="${subject.name}"></c:out></td>
			<td><c:out value="${subject.description}"></c:out></td>
			<td><c:out value="${subject.yearOfStudy}"></c:out></td>
			<td><c:out value="${subject.semester}"></c:out></td>
			<td><c:out value="${subject.professors}"></c:out></td>

		</tr>
	</table>
	<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/createsubject">Back
			to previous page</button>
	</form>
</body>
</html>