<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Show all subjects</title>
<link href="${pageContext.request.contextPath}/static/css/main.css"
    type="text/css" rel="stylesheet" >
<script type="text/javascript">
function confirmDelete(i){
	var c=confirm("Are you sure you want to delete the subject")
	if(c==true){
		window.location.pathname="${pageContext.request.contextPath}/deletesubject/"+i;
	}
	
}
</script>
</head>
<body>
	<h3>All subjects:</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>SubjectId</th>
				<th>Name</th>
				<th>Description</th>
				<th>YearOfStudy</th>
				<th>Semester</th>
				<th>Professor</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach var="tempSubject" items="${subjects}">
					<tr>
						<td>${tempSubject.subjectId}</td>
						<td>${tempSubject.name}</td>
						<td>${tempSubject.description}</td>
						<td>${tempSubject.yearOfStudy}</td>
						<td>${tempSubject.semester}</td>
						<td>${tempSubject.professors}</td>
						<td><a href="${pageContext.request.contextPath}/updatesubject/${tempSubject.subjectId}">updatesubject</a></td>
						<td><a onclick="confirmDelete(${tempSubject.subjectId})">Delete</a></td>
					</tr>
				</c:forEach>
			</tr>
		</tbody>
	</table>
				<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>