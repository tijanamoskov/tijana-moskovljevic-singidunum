<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<title>Professor created</title>
</head>
<body>
	<h3>Professor created</h3>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>ProfessorId</th>
				<th>FirstName</th>
				<th>LastName</th>
				<th>Email</th>
				<th>Address</th>
				<th>Phone</th>
				<th>ReelectionDate</th>
				<th>City</th>
				<th>Title</th>
			</tr>
		</thead>
		<tr>
			<td><c:out value="${professor.professorId}"></c:out></td>
			<td><c:out value="${professor.firstName}"></c:out></td>
			<td><c:out value="${professor.lastName}"></c:out></td>
			<td><c:out value="${professor.email}"></c:out></td>
			<td><c:out value="${professor.adress}"></c:out></td>
			<td><c:out value="${professor.phone}"></c:out></td>
			<td><c:out value="${professor.reelectionDate}"></c:out></td>
			<td><c:out value="${professor.city}"></c:out></td>
			<td><c:out value="${professor.title}"></c:out></td>
		</tr>
	</table>
	<br>
	<form>
		<button
			formaction="${pageContext.request.contextPath}/createprofessor">Back
			to previous page</button>
	</form>
</body>
</html>