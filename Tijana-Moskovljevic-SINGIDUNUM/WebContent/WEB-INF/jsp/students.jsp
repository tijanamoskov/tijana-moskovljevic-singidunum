<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<title>Show all students</title>
<script type="text/javascript">
function confirmDelete(i){
	var c=confirm("Are you sure you want to delete the student")
	if(c==true){
		window.location.pathname="${pageContext.request.contextPath}/deletestudent/"+i;
	}
}
</script>
</head>
<body>
	<h2>All students:</h2>
	<table border="2" cellpadding="5" cellspacing="2">
		<thead>
			<tr>
				<th>Student_id</th>
				<th>IndexNumber</th>
				<th>FirstName</th>
				<th>LastName</th>
				<th>Email</th>
				<th>Address</th>
				<th>City</th>
				<th>Phone</th>
				<th>CurrentYearOfStudy</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<c:forEach var="tempStudent" items="${students}">
					<tr>
						<td>${tempStudent.studentId}</td>
						<td>${tempStudent.indexNumber}</td>
						<td>${tempStudent.firstName}</td>
						<td>${tempStudent.lastName}</td>
						<td>${tempStudent.email}</td>
						<td>${tempStudent.adress}</td>
						<td>${tempStudent.city}</td>
						<td>${tempStudent.phone}</td>
						<td>${tempStudent.currentYearOfStudy}</td>
						<td><a
							href="${pageContext.request.contextPath}/updatestudent/${tempStudent.studentId}">updatestudent</a></td>
						<td><a onclick="confirmDelete(${tempStudent.studentId})">Delete</a></td>
					</tr>
				</c:forEach>
			</tr>
		</tbody>
	</table>
				<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/">BACK TO HOME PAGE</button>
	</form>
</body>
</html>