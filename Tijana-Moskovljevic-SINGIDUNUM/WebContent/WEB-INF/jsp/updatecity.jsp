<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update City</title>
<h3>Update City</h3>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
</head>
<body>
	<form action="${pageContext.request.contextPath}/doupdatecity"
		method="post">
		
			<tr>
				<td class=""></td>
				<td><input type="hidden" name="cityId" value="${city.cityId}"
					></td>
			</tr>
			<table border="2" cellpadding="5" cellspacing="2">
			<tr>
				<td class="label">Name</td>
				<td><input type="text" name="name" value="${city.name}"
					required></td>
			</tr>
			<tr>
				<td class="label">PostalCode</td>
				<td><input type="text" name="postalCode"
					value="${city.postalCode}" required></td>
			</tr>
			<tr>
				<td class="label"></td>
				<td class="control"><input type="submit" value="Update"></td>
			</tr>
		</table>
	</form>
		<br>
	<form>
		<button formaction="${pageContext.request.contextPath}/cities">Back
			to previous page</button>
	</form>
</body>
</html>