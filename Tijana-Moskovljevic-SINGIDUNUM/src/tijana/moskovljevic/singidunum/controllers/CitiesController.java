package tijana.moskovljevic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tijana.moskovljevic.singidunum.entities.City;
import tijana.moskovljevic.singidunum.services.CitiesService;

@Controller
public class CitiesController {
	
	 @Autowired
	 private CitiesService citiesService;
	 
	 @RequestMapping("/cities")
	 public String showAllCities(Model model) {
		 List<City>cities=citiesService.getAllCities();
		 model.addAttribute("cities",cities);
		 return "cities";
		 
	 }
	 @RequestMapping("/createcity")
		public String createCity() {
			return "createcity";
			
		}
	    @RequestMapping(value="/docreatecity",method=RequestMethod.POST)
		public String doCreate(Model model,@Valid City city,BindingResult result) {
			if(result.hasErrors()) {
				System.out.println("Form is not valid");
				
				List<ObjectError>errors=result.getAllErrors();
				for(ObjectError e:errors) {
					System.out.println(e.getDefaultMessage());
					
				return "createcity";
				}
		}
		else {
			System.out.println("Form validated successfully!");
		}
			
			citiesService.saveOrUpdateCity(city);
			model.addAttribute("city",city);
			return "citycreated";
		}
	    @RequestMapping(value="/deletecity/{id}")
		public String deleteCity(@PathVariable int id,Model model) {
	    citiesService.deleteCity(id);
		List<City>cities=citiesService.getAllCities();
		model.addAttribute("cities", cities);
		return "cities";
			
		}
	    @RequestMapping(value="/updatecity/{cityId}")
	    public String updateCity(@PathVariable int cityId,Model model) {
	    City city=citiesService.getCity(cityId);
	    model.addAttribute("city", city);
	    return "updatecity";
	    }
	    
	    @RequestMapping(value="/doupdatecity",method=RequestMethod.POST)
	    public String doUpdate(Model model,@Valid City city,BindingResult result) {
	    	System.out.println(city.getCityId());
	    	if(result.hasErrors()) {
	    		System.out.println("Form is not valid");
	    		List<ObjectError>errors=result.getAllErrors();
	    		for(ObjectError e:errors) {
	    			System.out.println(e.getDefaultMessage());
	    			return "updatecity";
	    		}
	    		
	    	}else {
	    		System.out.println("Form validated succesfully");
	    	}
	    	citiesService.saveOrUpdateCity(city);
	    	List<City>cities=citiesService.getAllCities();
	    	model.addAttribute("cities", cities);
			return "cities";
	    	
	    }
	

}
