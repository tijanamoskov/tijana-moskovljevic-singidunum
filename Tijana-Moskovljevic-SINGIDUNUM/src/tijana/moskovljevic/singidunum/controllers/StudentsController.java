package tijana.moskovljevic.singidunum.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import tijana.moskovljevic.singidunum.entities.City;
import tijana.moskovljevic.singidunum.entities.Student;
import tijana.moskovljevic.singidunum.entities.Subject;
import tijana.moskovljevic.singidunum.services.CitiesService;
import tijana.moskovljevic.singidunum.services.StudentsService;
import tijana.moskovljevic.singidunum.services.SubjectsService;
@Controller
public class StudentsController {
	@Autowired
	private StudentsService studentsServices;
	
	@Autowired
	private CitiesService citiesService;
	
	@Autowired
	private SubjectsService subjectsService;

	@RequestMapping("/students")
	public String showStudents(Model model) {
		List<Student> students = studentsServices.getAllStudents();
		model.addAttribute("students",students);
		return "students";		
	}
	
	@RequestMapping("/createstudent")
	public String createStudent(Model model){	
		List<City> cities = citiesService.getAllCities();
		model.addAttribute("cities",cities);
		List<Subject>subjects= subjectsService.getAllSubjects();
		model.addAttribute("subjects", subjects);
		return "createstudent";
	}
	@RequestMapping(value="/docreatestudent", method=RequestMethod.POST)
	public String doCreate(Model model,@Valid Student student,City city,@RequestParam ArrayList<Integer> listOfSubjects, BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError> errors = result.getAllErrors();
			for(ObjectError e: errors) {
				System.out.println(e.getDefaultMessage());
			return "createstudent";
			}
		}else {
			System.out.println("Form validated successsfully!");			
		}
		
		City newCity = citiesService.getCity(city.getCityId());
		student.setCity(newCity);
		Set<Subject>subjects=new HashSet<Subject>();
		for (Integer in : listOfSubjects) {
			subjects.add(subjectsService.getSubject(in.intValue()));
		}
		for (Subject subject : subjects) {
			System.out.println(subject);
		}
		student.setSubjects(subjects);
		studentsServices.saveOrUpdateStudent(student);
		model.addAttribute("student",student);
		return "studentcreated";
	}
	@RequestMapping(value="/deletestudent/{id}")
	public String deleteStudent(@PathVariable int id,Model model) {
		studentsServices.deleteStudent(id);
		List<Student>students=studentsServices.getAllStudents();
		model.addAttribute("students", students);
		return "students";
		
	}
	@RequestMapping(value="/updatestudent/{studentId}")
	public String updateStudent(@PathVariable int studentId,Model model) {
		Student student=studentsServices.getStudent(studentId);
		List<City> cities = citiesService.getAllCities();
		List<Subject>subjects=subjectsService.getAllSubjects();
		model.addAttribute("student", student);
		model.addAttribute("cities", cities);
		model.addAttribute("subjects",subjects);
		return "updatestudent";
	}
	@RequestMapping(value="/doupdatestudent", method=RequestMethod.POST)
	public String doUpdate(Model model,@Valid Student student,City city,@RequestParam ArrayList<Integer> listOfSubjects,BindingResult result) {
		System.out.println(student.getStudentId());
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError>errors=result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
				return "updatestudent";
			}
		}else {
			System.out.println("Form validated successfully");
		}
		Set<Subject>subj=new HashSet<Subject>();
		 for(Integer i:listOfSubjects) {
			 subj.add(subjectsService.getSubject(i.intValue()));
		 }
		City city1=citiesService.getCity(city.getCityId());
		student.setCity(city1);
		studentsServices.saveOrUpdateStudent(student);
		student.setSubjects(subj);
		studentsServices.saveOrUpdateStudent(student);
		List<Student>students=studentsServices.getAllStudents();
		model.addAttribute("students",students);
		return "students";
		
	}
	
}

