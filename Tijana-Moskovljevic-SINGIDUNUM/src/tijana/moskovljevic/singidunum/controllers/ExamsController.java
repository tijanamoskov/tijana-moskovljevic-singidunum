package tijana.moskovljevic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tijana.moskovljevic.singidunum.entities.Exam;
import tijana.moskovljevic.singidunum.entities.Professor;
import tijana.moskovljevic.singidunum.entities.Subject;
import tijana.moskovljevic.singidunum.services.ExamsService;
import tijana.moskovljevic.singidunum.services.ProfessorsService;
import tijana.moskovljevic.singidunum.services.SubjectsService;

@Controller
public class ExamsController {
	
	@Autowired
	private ExamsService examsService;
	
	@Autowired
	private ProfessorsService professorsService;
	
	@Autowired
	private SubjectsService subjectsService;
	
	
	@RequestMapping("/exams")
	private String showAllExams(Model model) {
		List<Exam>exams=examsService.getAllExams();
		model.addAttribute("exams",exams);
		return "exams";	
	}
	@RequestMapping("/createexam")
	public String createExam(Model model) {
		List<Subject>subjects=subjectsService.getAllSubjects();
		List<Professor>professors=professorsService.getAllProfessor();
		model.addAttribute("subjects",subjects);
		model.addAttribute("professors",professors);
		return "createexam";
		
	}
	
	@RequestMapping(value="docreateexam",method=RequestMethod.POST)
	public String doCreate(Model model,@Valid Exam exam,Subject subject,Professor professor,BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError>errors=result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
				return "createexam";
			}
		}else {
			System.out.println("Form validated successfully");
		}
		Subject newSubject=subjectsService.getSubject(subject.getSubjectId());
		exam.setSubject(newSubject);
		Professor newProfessor=professorsService.getProfessor(professor.getProfessorId());
		exam.setProfessor(newProfessor);
		examsService.saveOrUpdateExam(exam);
		model.addAttribute("exam", exam);
		return "examcreated";
		
	}
	 @RequestMapping(value="/deleteexam/{id}")
	 public String deleteExam(@PathVariable int id,Model model) {
     examsService.deleteExam(id);
	 List<Exam>exams=examsService.getAllExams();
	 model.addAttribute("exams", exams);
	 return "exams";
			
	}
	 @RequestMapping(value="updateexam/{examId}")
	 public String updateExam(@PathVariable int examId,Model model) {
		 Exam exam=examsService.getExam(examId);
		 List<Professor>professors=professorsService.getAllProfessor();
		 model.addAttribute("professors",professors);
		 List<Subject>subjects=subjectsService.getAllSubjects();
		 model.addAttribute("subjects",subjects);
		 model.addAttribute("exam",exam);
		 return "updateexam";
	 }
	 @RequestMapping(value="/doupdateexam",method=RequestMethod.POST)
	 public String doUpdate(Model model,@Valid Exam exam,Subject subject,Professor professor,BindingResult result) {
		System.out.println(exam.getExamId());
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			List<ObjectError>errors=result.getAllErrors();
			for(ObjectError e:errors) {
			System.out.println(e.getDefaultMessage());
		    return "updateexam";
		}
		 
	 }else {
		 System.out.println("Form validated succesfully");
	 }
		Subject s=subjectsService.getSubject(subject.getSubjectId());
		exam.setSubject(s);
		Professor p=professorsService.getProfessor(professor.getProfessorId());
		exam.setProfessor(p);
		examsService.saveOrUpdateExam(exam);
		List<Exam>exams=examsService.getAllExams();
		model.addAttribute("exams",exams);
		return "exams";

}
	
}
