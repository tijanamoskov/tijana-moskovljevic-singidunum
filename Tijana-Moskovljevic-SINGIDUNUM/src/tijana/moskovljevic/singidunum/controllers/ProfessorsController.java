package tijana.moskovljevic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tijana.moskovljevic.singidunum.entities.City;
import tijana.moskovljevic.singidunum.entities.Professor;
import tijana.moskovljevic.singidunum.entities.Title;
import tijana.moskovljevic.singidunum.services.CitiesService;
import tijana.moskovljevic.singidunum.services.ProfessorsService;
import tijana.moskovljevic.singidunum.services.TitlesService;

@Controller
public class ProfessorsController {
	
	@Autowired
	private ProfessorsService professorsService;
	
	@Autowired
	private CitiesService citiesService;
	
	@Autowired
	private TitlesService titlesService;
	
	@RequestMapping("/professors")
	public String showAllProfessors(Model model) {
		List<Professor>professors=professorsService.getAllProfessor();
		model.addAttribute("professors",professors);
		return "professors";
		
	}
	 @RequestMapping("/createprofessor")
	 public String createProfessor(Model model){   
	 List<City> cities = citiesService.getAllCities();
	 List<Title>titles=titlesService.getAllTitles();
	 model.addAttribute("cities",cities);
	 model.addAttribute("titles",titles);
	 return "createprofessor";
	    }
	 
	 @RequestMapping(value="/docreateprofessor",method=RequestMethod.POST)
		public String doCreate(Model model,@Valid Professor professor,City city,Title title,BindingResult result) {
			if(result.hasErrors()) {
				System.out.println("Form is not valid");				
				List<ObjectError>errors=result.getAllErrors();
				for(ObjectError e:errors) {
					System.out.println(e.getDefaultMessage());
					
				return "createprofessor";
		}
	}   else {
			System.out.println("Form validated successfully!");
		}	
			System.out.println(professor);
			City newCity = citiesService.getCity(city.getCityId());
			professor.setCity(newCity);
			Title newTitle=titlesService.getTitle(title.getTitleId());
			professor.setTitle(newTitle);
			professorsService.saveOrUpdateProfessor(professor);
			model.addAttribute("professor",professor);
			return "professorcreated";
		}
	 @RequestMapping(value="/deleteprofessor/{id}")
	 public String deleteProfessor(@PathVariable int id,Model model) {
     professorsService.deleteProfessor(id);
	 List<Professor>professors=professorsService.getAllProfessor();
	 model.addAttribute("professors", professors);
	 return "professors";
			
	}
	 @RequestMapping(value="updateprofessor/{professorId}")
	 public String updateProfessor(@PathVariable int professorId,Model model) {
		 Professor professor=professorsService.getProfessor(professorId);
		 List<City> cities = citiesService.getAllCities();
		 List<Title>titles=titlesService.getAllTitles();
		 model.addAttribute("professor", professor);
		 model.addAttribute("cities", cities);
		 model.addAttribute("titles", titles);
		 return "updateprofessor";
	 }
	 @RequestMapping(value="/doupdateprofessor", method=RequestMethod.POST)
	 public String doUpdate(Model model,@Valid Professor professor,BindingResult result,City city, Title title) {
		 System.out.println(professor.getProfessorId());
		 if(result.hasErrors()) {
			 System.out.println("Form is not valid");
			 List<ObjectError>errors=result.getAllErrors();
			 for(ObjectError e:errors) {
				 System.out.println(e.getDefaultMessage());
				 return "updateprofessor";
			 }
		 }else {
			 System.out.println("Form validated successfully");
		 }
		 City c=citiesService.getCity(city.getCityId());
		 professor.setCity(c);
		 Title t=titlesService.getTitle(title.getTitleId());
		 professor.setTitle(t);
		 professorsService.saveOrUpdateProfessor(professor);
		 List<Professor>professors=professorsService.getAllProfessor();
		 model.addAttribute("professors", professors);
		 return "professors";
	 }

}
