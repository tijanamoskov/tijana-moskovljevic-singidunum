package tijana.moskovljevic.singidunum.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tijana.moskovljevic.singidunum.entities.City;
import tijana.moskovljevic.singidunum.entities.Title;
import tijana.moskovljevic.singidunum.services.TitlesService;

@Controller
public class TitlesController {
	
	@Autowired
	private TitlesService titlesService;
	
	@RequestMapping("titles")
	public String showAllTitles(Model model) {
		List<Title>titles=titlesService.getAllTitles();
		model.addAttribute("titles", titles);
		return "titles";
		
	}
	@RequestMapping("/createtitle")
	public String createTitle() {
		return "createtitle";
		
	}
	@RequestMapping(value="/docreatetitle",method=RequestMethod.POST)
	public String doCreate(Model model,@Valid Title title,BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			
			List<ObjectError>errors=result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
				
			return "createtitle";
	}
	} else {
		System.out.println("Form validated successfully!");
	}
		
		titlesService.saveOrUpdateTitle(title);
		model.addAttribute("title",title);
		return "titlecreated";
	}
	 @RequestMapping(value="/deletetitle/{id}")
	 public String deleteTitle(@PathVariable int id,Model model) {
     titlesService.deleteTitle(id);
	 List<Title>titles=titlesService.getAllTitles();
	 model.addAttribute("titles", titles);
	 return "updatetitle";
			
		}
	 @RequestMapping(value="/updatetitle/{titleId}")
	 public String updateTitle(@PathVariable int titleId,Model model) {
		 Title title=titlesService.getTitle(titleId);
		 model.addAttribute("title", title);
		  return "updatetitle";
		 
	 }
	 @RequestMapping(value="/doupdatetitle",method=RequestMethod.POST)
	 public String doUpdate(Model model,@Valid Title title,BindingResult result) {
		 System.out.println(title.getTitleId());
		 if(result.hasErrors()) {
			 System.out.println("Form is not valid");
			 List<ObjectError>errors=result.getAllErrors();
			 for(ObjectError e:errors) {
				 System.out.println(e.getDefaultMessage());
				 return "updatetitle";
			 }
		 }
		 else {
			 System.out.println("Form validated succesfully");
		 }
		 titlesService.saveOrUpdateTitle(title);
		 List<Title>titles=titlesService.getAllTitles();
		 model.addAttribute("titles", titles);
		 return "titles";
		 
	 }


}
