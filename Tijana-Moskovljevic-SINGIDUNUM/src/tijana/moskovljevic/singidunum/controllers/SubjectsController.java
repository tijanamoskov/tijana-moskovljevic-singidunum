package tijana.moskovljevic.singidunum.controllers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import tijana.moskovljevic.singidunum.entities.Professor;
import tijana.moskovljevic.singidunum.entities.Subject;
import tijana.moskovljevic.singidunum.services.ProfessorsService;
import tijana.moskovljevic.singidunum.services.SubjectsService;

@Controller
public class SubjectsController {
	
	@Autowired
	private SubjectsService subjectsService;
	
	@Autowired
	private ProfessorsService professorsService;
	
	@RequestMapping("/subjects")
	public String showSubjects(Model model) {
		List<Subject>subjects=subjectsService.getAllSubjects();
		model.addAttribute("subjects", subjects);
		return "subjects";
	}
	
	@RequestMapping("/createsubject")
	public String createSubject(Model model) {
	List<Professor>professors= professorsService.getAllProfessor();
	model.addAttribute("professors", professors);
	return "createsubject";
		
	}
	@RequestMapping(value="/docreatesubject",method=RequestMethod.POST)
	public String doCreate(Model model,@Valid Subject subject,@RequestParam ArrayList<Integer> listOfProfessors,BindingResult result) {
		if(result.hasErrors()) {
			System.out.println("Form is not valid");
			
			List<ObjectError>errors=result.getAllErrors();
			for(ObjectError e:errors) {
				System.out.println(e.getDefaultMessage());
				
			return "createsubject";
			}
	}
	else {
		System.out.println("Form validated successfully!");
	}
		Set<Professor>professors=new HashSet<Professor>();
		for (Integer in : listOfProfessors) {
			professors.add(professorsService.getProfessor(in.intValue()));
		}
		for (Professor professor : professors) {
			System.out.println(professor);
		}
		subject.setProfessors(professors);
		subjectsService.saveOrUpdateSubject(subject);
		model.addAttribute("subject",subject);
		return "subjectcreated";
	}
	 @RequestMapping(value="/deletesubject/{id}")
	 public String deleteSubject(@PathVariable int id,Model model) {
     subjectsService.deleteSubject(id);
	 List<Subject>subjects=subjectsService.getAllSubjects();
	 model.addAttribute("subjects", subjects);
	 return "subjects";
			
		}
	 @RequestMapping(value="/updatesubject/{subjectId}")
	 public String updateSubject(@PathVariable int subjectId,Model model) {
		 Subject subject=subjectsService.getSubject(subjectId);
		 List<Professor>professors=professorsService.getAllProfessor();
		 model.addAttribute("subject", subject);
		 model.addAttribute("professors", professors);
		 return "updatesubject";
	 }
	 @RequestMapping(value="/doupdatesubject",method=RequestMethod.POST)
	 public String doUpdate(Model model,@Valid Subject subject,@RequestParam ArrayList<Integer>listOfProf,BindingResult result) {
		 System.out.println(subject.getSubjectId());
		 if(result.hasErrors()) {
			 System.out.println("Form is not valid");
			 List<ObjectError>errors=result.getAllErrors();
			 for(ObjectError e:errors) {
				 System.out.println(e.getDefaultMessage());
				 return "updatesubject";
			 }
		 }else {
			 System.out.println("Form validated successfully ");
		 }
		 Set<Professor> prof= new HashSet<Professor>();
		 for(Integer i:listOfProf) {
			 prof.add(professorsService.getProfessor(i.intValue()));
		 } 
		 subject.setProfessors(prof);
		 subjectsService.saveOrUpdateSubject(subject);
		 List<Subject>subjects=subjectsService.getAllSubjects();
		 model.addAttribute("subjects", subjects);
		 return "subjects";
	 }
	 

}
