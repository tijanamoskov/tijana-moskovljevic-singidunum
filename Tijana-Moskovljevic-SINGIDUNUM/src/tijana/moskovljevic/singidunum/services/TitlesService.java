package tijana.moskovljevic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tijana.moskovljevic.singidunum.dao.TitlesDAO;
import tijana.moskovljevic.singidunum.entities.Title;

@Service
public class TitlesService {
	
	@Autowired
    TitlesDAO titlesDao;
	
	public List<Title>getAllTitles(){
		return titlesDao.getAllTitles();
		
	}
	
	public Title getTitle(int id) {
		return titlesDao.getTitle(id);
	}
	public Title saveOrUpdateTitle(Title title) {
		return titlesDao.saveOrUpdateTitle(title);
		
	}
	public void deleteTitle(int id) {
	  titlesDao.deleteTitle(id);
		
	}
}
