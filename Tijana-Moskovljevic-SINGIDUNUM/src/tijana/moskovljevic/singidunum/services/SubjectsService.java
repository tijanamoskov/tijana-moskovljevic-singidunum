package tijana.moskovljevic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tijana.moskovljevic.singidunum.dao.SubjectsDAO;
import tijana.moskovljevic.singidunum.entities.Subject;

@Service
public class SubjectsService {
	
	@Autowired
	SubjectsDAO subjectsDao;
	
	public List<Subject> getAllSubjects(){
		return subjectsDao.getAllSubjects() ;
	}
	
	public Subject getSubject(int id) {
		return subjectsDao.getSubject(id);
	}
	public Subject saveOrUpdateSubject(Subject subject) {
		return subjectsDao.saveOrUpdateSubject(subject);
	}
	public void deleteSubject(int id) {
		subjectsDao.deleteSubject(id);
	}

}
