package tijana.moskovljevic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tijana.moskovljevic.singidunum.dao.ExamsDAO;
import tijana.moskovljevic.singidunum.entities.Exam;

@Service
public class ExamsService {

	@Autowired
	ExamsDAO examsDao;
	
	public List<Exam>getAllExams(){
		return examsDao.getAllExams();
		
	}
	public Exam getExam(int id) {
		return examsDao.getExam(id);
		
	}
	public Exam saveOrUpdateExam(Exam exam) {
		return examsDao.saveOrUpdateExam(exam);
	}
	public void deleteExam(int id) {
		examsDao.deleteExam(id);
	}
}
