package tijana.moskovljevic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tijana.moskovljevic.singidunum.dao.ProfessorsDAO;
import tijana.moskovljevic.singidunum.entities.Professor;

@Service
public class ProfessorsService {
	
	@Autowired
	ProfessorsDAO professorsDao;
	
	public List<Professor>getAllProfessor(){
		return professorsDao.getAllProfessor();
		
	}
	public Professor getProfessor(int id) {
		return professorsDao.getProfessor(id);
	}
	public Professor saveOrUpdateProfessor(Professor professor) {
		return professorsDao.saveOrUpdateProfessor(professor);
		
	}
	public void deleteProfessor(int id) {
		 professorsDao.deleteProfessor(id);
		
	}

}
