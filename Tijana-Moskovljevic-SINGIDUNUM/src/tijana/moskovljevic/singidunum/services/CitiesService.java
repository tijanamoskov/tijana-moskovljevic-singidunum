package tijana.moskovljevic.singidunum.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tijana.moskovljevic.singidunum.dao.CitiesDAO;
import tijana.moskovljevic.singidunum.entities.City;

@Service
public class CitiesService {
	@Autowired
	CitiesDAO citiesDao;
	
	public List<City>getAllCities(){
		return citiesDao.getAllCities();
	}
	public City getCity(int id) {
		return citiesDao.getCity(id);
	}
	public City saveOrUpdateCity(City city) {
		return citiesDao.saveOrUpdateCity(city);
	}
	public void deleteCity(int id) {
		citiesDao.deleteCity(id);
	}

}
