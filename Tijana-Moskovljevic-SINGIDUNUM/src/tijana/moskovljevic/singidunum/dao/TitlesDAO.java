package tijana.moskovljevic.singidunum.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tijana.moskovljevic.singidunum.entities.Title;

@Component("titlesDao")
@Transactional
public class TitlesDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Title>getAllTitles(){
		return session.getCurrentSession().createQuery("FROM Title").list();
		
	}
	public Title getTitle(int id) {
		return session.getCurrentSession().get(Title.class, id);
	}
	public Title saveOrUpdateTitle(Title title) {
		session.getCurrentSession().saveOrUpdate(title);
		return title;
	}
	public void deleteTitle(int id) {
		
		session.getCurrentSession().delete(session.getCurrentSession().get(Title.class, id));

}
}
