package tijana.moskovljevic.singidunum.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tijana.moskovljevic.singidunum.entities.Subject;

@Component("subjectsDao")
@Transactional
public class SubjectsDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Subject>getAllSubjects(){
		return session.getCurrentSession().createQuery("FROM Subject").list();
	}
	public Subject getSubject(int id) {
		return session.getCurrentSession().get(Subject.class, id);
		
	}
	public Subject saveOrUpdateSubject(Subject subject) {
		session.getCurrentSession().saveOrUpdate(subject);
		return subject;
		
	}
	public void deleteSubject(int id) {
		
		session.getCurrentSession().delete(session.getCurrentSession().get(Subject.class, id));
		}
}

