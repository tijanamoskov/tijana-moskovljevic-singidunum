package tijana.moskovljevic.singidunum.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tijana.moskovljevic.singidunum.entities.Exam;

@Component("examsDao")
@Transactional
public class ExamsDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Exam>getAllExams(){
		return session.getCurrentSession().createQuery("FROM Exam").list();
		
	}
	public Exam getExam(int id) {
		return session.getCurrentSession().get(Exam.class, id);
	}
	public Exam saveOrUpdateExam(Exam exam) {
		session.getCurrentSession().saveOrUpdate(exam);
		return exam;
		
	}
	public void deleteExam(int id) {
		
		session.getCurrentSession().delete(session.getCurrentSession().get(Exam.class, id));

}
}
