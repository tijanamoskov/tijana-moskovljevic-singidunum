package tijana.moskovljevic.singidunum.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tijana.moskovljevic.singidunum.entities.City;

@Component("citiesDao")
@Transactional
public class CitiesDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<City>getAllCities(){
		return session.getCurrentSession().createQuery("FROM City").list();
		
	}
	public City getCity(int id) {
		return session.getCurrentSession().get(City.class, id);
	}
	public City saveOrUpdateCity(City city) {
		session.getCurrentSession().saveOrUpdate(city);
		return city;
	}
	public void deleteCity(int id) {
	
		session.getCurrentSession().delete(session.getCurrentSession().get(City.class, id));

		
	}

}
