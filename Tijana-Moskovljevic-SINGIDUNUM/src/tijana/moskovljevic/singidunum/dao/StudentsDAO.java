package tijana.moskovljevic.singidunum.dao;

import java.util.List;
import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tijana.moskovljevic.singidunum.entities.Student;
@Component("studentsDao")
@Transactional
public class StudentsDAO {
	
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Student> getAllStudents(){
		return session.getCurrentSession().createQuery("FROM Student").list();
	}
	
	public Student getStudent(int id) {
		return session.getCurrentSession().get(Student.class, id);
		
	}
	
	public Student saveOrUpdateStudent(Student student) {
		session.getCurrentSession().saveOrUpdate(student);
		return student;
		
	}
	public void deleteStudent(int id) {
		session.getCurrentSession().delete(session.getCurrentSession().get(Student.class, id));
		}
	
	}
	    	
	    
