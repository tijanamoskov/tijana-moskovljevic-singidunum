package tijana.moskovljevic.singidunum.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tijana.moskovljevic.singidunum.entities.Professor;

@Component("professorsDao")
@Transactional
public class ProfessorsDAO {
	
	@Autowired
	SessionFactory session;
	
	@SuppressWarnings("unchecked")
	public List<Professor>getAllProfessor(){
		return session.getCurrentSession().createQuery("from Professor").list();
		
	}
	public Professor getProfessor(int id) {
		return session.getCurrentSession().get(Professor.class, id);
	}
	public Professor saveOrUpdateProfessor(Professor professor) {
		session.getCurrentSession().saveOrUpdate(professor);
		return professor;
		
	}
	public void deleteProfessor(int id) {
	
		session.getCurrentSession().delete(session.getCurrentSession().get(Professor.class, id));
	

}
}
