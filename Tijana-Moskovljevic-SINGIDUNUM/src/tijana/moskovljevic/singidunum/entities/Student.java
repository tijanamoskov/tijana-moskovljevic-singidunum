package tijana.moskovljevic.singidunum.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
@Entity
@Table(name="students")
public class Student {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Student_id")	
	private int studentId;
	
	@NotNull(message="IndexNumber cannot be null")
	@Column(name="IndexNumber",unique=true)
	@Size(min=10,message="Minimum number of characters for IndexNumber is 10")
	private String indexNumber;
	
	@Column(name="FirstName")
	@NotNull(message="First Name cannot be null")
	@Size(min=3, max=30,message="First name must be between 3 and 30 characters")
	private String firstName;
	
	@Column(name="LastName")
	@NotNull(message="Last Name cannot be null")
	@Size(min=3, max=30,message="Last name must be between 3 and 30 characters")
	private String lastName;
	
	@Column(name="Email",unique=true)
	@Email(message="Email adress must have @ characters")
	@Size(max=30,message="Max number of characters for Email is 30")
	private String email;
	
	@Column(name="Adress")
	@Size(min=3, max=50,message="Minimum number of characters for Adress is 3")
	private String adress;
	
	@ManyToOne
	private City city;	
	
	@Column(name="Phone")
	@Size(min=6, max=15,message="Minimum number of characters for phone number is 6")
	private String phone;
	
	@Column(name="CurrentYearOfStudy")
	@NotNull(message="CurrentYearOfStudy cannot be null")
	@Max(7)
	private int currentYearOfStudy;
	
	@ManyToMany(mappedBy="students")
	private Set<Subject>subjects=new HashSet<Subject>();
	
	@ManyToMany(mappedBy="students")
	private Set<Exam>exams=new HashSet<Exam>();
	
	public Student() {
		
	}

	public Student(int studentId,
			@NotNull(message = "IndexNumber cannot be null") @Size(min = 10, message = "Minimum number of characters for IndexNumber is 10") String indexNumber,
			@NotNull(message = "First Name cannot be null") @Size(min = 3, max = 30, message = "First name must be between 3 and 30 characters") String firstName,
			@NotNull(message = "Last Name cannot be null") @Size(min = 3, max = 30, message = "Last name must be between 3 and 30 characters") String lastName,
			@Email(message = "Email adress must have @ characters") @Size(max = 30, message = "Max number of characters for Email is 30") String email,
			@Size(min = 3, max = 50, message = "Minimum number of characters for Adress is 3") String adress, City city,
			@Size(min = 6, max = 15, message = "Minimum number of characters for phone number is 6") String phone,
			@NotNull(message = "CurrentYearOfStudy cannot be null") @Max(7) int currentYearOfStudy,
			Set<Subject> subjects, Set<Exam> exams) {
		super();
		this.studentId = studentId;
		this.indexNumber = indexNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.adress = adress;
		this.city = city;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
		this.subjects = subjects;
		this.exams = exams;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getIndexNumber() {
		return indexNumber;
	}

	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}

	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}

	public Set<Subject> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}

	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", indexNumber=" + indexNumber + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", email=" + email + ", adress=" + adress + ", phone="
				+ phone + ", currentYearOfStudy=" + currentYearOfStudy + ",city="+city+"]";
	}

	
	

	
	
	
	
	
	
	
	
	
	

}
