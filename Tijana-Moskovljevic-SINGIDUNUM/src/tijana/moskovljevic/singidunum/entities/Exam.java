package tijana.moskovljevic.singidunum.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="exams")
public class Exam {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="exam_id")
	private int examId;
	
	@Column(name="ExamDate")
	@NotNull
	private Date examdate;
	
	@ManyToOne
	private Professor professor;
	
	@ManyToOne
	private Subject subject;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="students_exams", joinColumns= @JoinColumn(name="exam_id"),inverseJoinColumns = @JoinColumn(name="Student_id"))
	private Set<Student>students=new HashSet<Student>();

	public Exam() {
		
	}

	public Exam(int examId, @NotNull Date examdate, Professor professor, Subject subject, Set<Student> students) {
		super();
		this.examId = examId;
		this.examdate = examdate;
		this.professor = professor;
		this.subject = subject;
		this.students = students;
	}

	public int getExamId() {
		return examId;
	}

	public void setExamId(int examId) {
		this.examId = examId;
	}

	public Date getExamdate() {
		return examdate;
	}

	public void setExamdate(Date examdate) {
		this.examdate = examdate;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	@Override
	public String toString() {
		return "Exam [examId=" + examId + ", examdate=" + examdate + "]";
	}
	
}
