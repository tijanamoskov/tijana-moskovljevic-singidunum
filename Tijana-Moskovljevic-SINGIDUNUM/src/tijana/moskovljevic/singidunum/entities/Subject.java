package tijana.moskovljevic.singidunum.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.ConstraintMode;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name="subjects")
public class Subject {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Subject_id")	
	private int subjectId;
	
	@Column(name="Name")
	@NotNull(message="Subject Name cannot be null")
	@Size(min=3, max=30,message="Subject must be between 3 and 30 characters")
	private String name;
	
	@Column(name="Description")
	@Size(max=200,message="Description cannot have more than 200 characters")
	private String description;
	
	@Column(name="YearOfStudy")
	@Range(min = 1, max = 9, message = "Please select only numbers from 1 to 9")
	private int yearOfStudy;
	

	@Column(name="Semester")
	@Size(max=10,message="Semester cannot have more than 10 characters")
	private String semester;
	
	
	@ManyToMany(fetch = FetchType.LAZY,cascade ={CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST},targetEntity = Professor.class)
	@JoinTable(name="professors_subjects", joinColumns= @JoinColumn(name="Subject_id",nullable = false,updatable = false),inverseJoinColumns = @JoinColumn(name="Professor_id",nullable = false, updatable = false),
	foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Professor>professors=new HashSet<Professor>();
	
	@ManyToMany(fetch = FetchType.LAZY,cascade ={CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST},targetEntity = Student.class)
	@JoinTable(name="students_subjects", joinColumns= @JoinColumn(name="Subject_id",nullable = false,updatable = false),inverseJoinColumns = @JoinColumn(name="Student_id",nullable = false, updatable = false),
    foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Student>students=new HashSet<Student>();
	
	@OneToMany(mappedBy="subject")
	private List<Exam> exams=new ArrayList<Exam>();

	public Subject() {
		
	}

	public Subject(int subjectId,
			@NotNull(message = "Subject Name cannot be null") @Size(min = 3, max = 30, message = "Subject must be between 3 and 30 characters") String name,
			@Size(max = 200, message = "Description cannot have more than 200 characters") String description,
			@Max(1) int yearOfStudy,
			@Size(max = 10, message = "Semester cannot have more than 10 characters") String semester,
			Set<Professor> professors, Set<Student> students, List<Exam> exams) {

		this.subjectId = subjectId;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
		this.professors = professors;
		this.students = students;
		this.exams = exams;
	}

	public int getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getYearOfStudy() {
		return yearOfStudy;
	}

	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Set<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(Set<Professor> professors) {
		this.professors = professors;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	public List<Exam> getExams() {
		return exams;
	}

	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}

	@Override
	public String toString() {
		return "Subject [subjectId=" + subjectId + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + ", professors=" + professors + "]";
	}

	


	

}
