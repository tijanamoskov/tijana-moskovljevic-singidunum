package tijana.moskovljevic.singidunum.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="titles")
public class Title {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="title_id")
	private int titleId;
	
	@NotNull
	@Column(name="Name")
	private String name;
	
	@OneToMany(mappedBy="title")
	private List<Professor> professors=new ArrayList<Professor>();

	public Title() {
	
	}

	public Title(int titleId, @NotNull String name, List<Professor> professors) {
	
		this.titleId = titleId;
		this.name = name;
		this.professors = professors;
	}

	public int getTitleId() {
		return titleId;
	}

	public void setTitleId(int titleId) {
		this.titleId = titleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(List<Professor> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return  name;
	}
	

	
	

}
