package tijana.moskovljevic.singidunum.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.ForeignKey;
import javax.persistence.ConstraintMode;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="professors")
public class Professor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="professor_id")
	private int professorId;
	
	@Column(name="FirstName")
	@NotNull(message="First Name cannot be null")
	@Size(min=3, max=30,message="First name must be between 3 and 30 characters")
	private String firstName;
	
	@Column(name="LastName")
	@NotNull(message="Last Name cannot be null")
	@Size(min=3, max=30,message="Last name must be between 3 and 30 characters")
	private String lastName;
	
	@Column(name="Email",unique=true)
	@Size(max=30)
	@Email(message="Email adress must have @ characters")
	private String email;
	
	@Column(name="Adress")
	@Size(min=3, max=50,message="Adress must be between 3 and 50 characters")
	private String adress;
	
	@Column(name="Phone")
	@Size(min=6, max=15,message="Phone number must be between 6 and 15 characters")
	private String phone;
	
	@Column(name="ReelectionDate")
	@NotNull
	private Date reelectionDate;
	
	@ManyToOne
	private City city;
	
	@ManyToOne
	private Title title;
	
	
	@ManyToMany(fetch = FetchType.LAZY,cascade ={CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.PERSIST},targetEntity = Subject.class)
	@JoinTable(name="professors_subjects", joinColumns= @JoinColumn(name="professor_id",nullable = false,updatable = false),inverseJoinColumns = @JoinColumn(name="Subject_id",nullable = false, updatable = false),
	foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT),inverseForeignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
	private Set<Subject>subjects=new HashSet<Subject>();
	
	@OneToMany(mappedBy="professor")
	private List<Exam> exams=new ArrayList<Exam>(); 
	
	

	public Professor() {
	
	}



	public Professor(int professorId,
			@NotNull(message = "First Name cannot be null") @Size(min = 3, max = 30, message = "First name must be between 3 and 30 characters") String firstName,
			@NotNull(message = "Last Name cannot be null") @Size(min = 3, max = 30, message = "Last name must be between 3 and 30 characters") String lastName,
			@Size(max = 30) @Email(message = "Email adress must have @ characters") String email,
			@Size(min = 3, max = 50, message = "Adress must be between 3 and 50 characters") String adress,
			@Size(min = 6, max = 15, message = "Phone number must be between 6 and 15 characters") String phone,
			@NotNull Date reelectionDate, City city, @NotNull Title title, Set<Subject> subjects, List<Exam> exams) {
	
		this.professorId = professorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.adress = adress;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.city = city;
		this.title = title;
		this.subjects = subjects;
		this.exams = exams;
	}



	public int getProfessorId() {
		return professorId;
	}



	public void setProfessorId(int professorId) {
		this.professorId = professorId;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getAdress() {
		return adress;
	}



	public void setAdress(String adress) {
		this.adress = adress;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public Date getReelectionDate() {
		return reelectionDate;
	}



	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}



	public City getCity() {
		return city;
	}



	public void setCity(City city) {
		this.city = city;
	}



	public Title getTitle() {
		return title;
	}



	public void setTitle(Title title) {
		this.title = title;
	}



	public Set<Subject> getSubjects() {
		return subjects;
	}



	public void setSubjects(Set<Subject> subjects) {
		this.subjects = subjects;
	}



	public List<Exam> getExams() {
		return exams;
	}



	public void setExams(List<Exam> exams) {
		this.exams = exams;
	}



	@Override
	public String toString() {
		return  firstName + " " + lastName;
	}

	

	

	
	
	
	
	

}
