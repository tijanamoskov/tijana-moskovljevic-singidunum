package tijana.moskovljevic.singidunum.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name="cities")
public class City {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="city_id")
	private int cityId;
	
	@NotNull(message="City Name cannot be null")
	@Column(name="Name")
	private String name;
	
	@Column(name="PostalCode")
	private String postalCode;
	
	@OneToMany(mappedBy="city")
	private List<Student> students=new ArrayList<Student>();
	
	@OneToMany(mappedBy="city")
	private List<Professor> professors=new ArrayList<Professor>();
	
	public City() {
		
	}

	public City(int cityId, @NotNull(message = "City Name cannot be null") String name, String postalCode,
			List<Student> students, List<Professor> professors) {
		super();
		this.cityId = cityId;
		this.name = name;
		this.postalCode = postalCode;
		this.students = students;
		this.professors = professors;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public List<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(List<Professor> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return  name + ", postalCode=" + postalCode;
	}

	
	

	
	
	
	

}
